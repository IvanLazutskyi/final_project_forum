﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class PagedTopicModel
    {
        public IEnumerable<TopicDTO> Topics { get; set; }
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
    }
}
