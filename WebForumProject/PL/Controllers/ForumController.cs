﻿using BLL.DTO;
using BLL.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    /// <summary>
    /// Controller for all Forum actions
    /// </summary>
    public class ForumController : Controller
    {
        private IUserService _userService;
        private ITopicService _topicService;
        private IMessageService _messageService;
        
        public ForumController(IUserService userService, ITopicService topicService, IMessageService messageService)
        {
            _userService = userService;
            _topicService = topicService;
            _messageService = messageService;
        }
        /// <summary>
        /// Displays All Topics
        /// </summary>
        /// <returns>View and model with all topics</returns>
        public ActionResult AllTopics(int currentPage = 1)
        {
            var topics =_topicService.GetPagedTopics(currentPage);
            return View(topics);
        }
        /// <summary>
        /// Displays choosen Topic
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DisplayTopic(int id, int currentPage = 1)
        {
            var topic = await _topicService.GetById(id, currentPage);
            
            if(topic==null)
            {
                return new HttpStatusCodeResult(404);
            }
            return View("Topic", topic);

        }
        /// <summary>
        /// Redirects to Topic Creation Form
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult AddTopicForm()
        {
            return View();
        }
        /// <summary>
        /// Adds topic 
        /// </summary>
        /// <param name="topicDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddTopic(TopicFormViewModel topicDTO)
        {
            var userName = User.Identity.Name;
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if(String.IsNullOrEmpty(topicDTO.Text))
                {
                    ModelState.AddModelError("", "Message is null!");
                }
                if (String.IsNullOrEmpty(topicDTO.Name))
                {
                    ModelState.AddModelError("", "Topic is null!");
                }
                await _topicService.AddAsync(topicDTO, userId, userName);
            }
            return RedirectToAction("AllTopics");
        }
        /// <summary>
        /// Adds new message to topic
        /// </summary>
        /// <param name="newMessage"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddNewMessage(NewMessageFormModel newMessage)
        {
           
            var userName = User.Identity.Name;
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (newMessage.Text==null)
                {
                    ModelState.AddModelError("", "Message should be filled");
                }
                if( newMessage.TopicId==0)
                {
                    ModelState.AddModelError("", "Empty Topic Id!");
                }
                await _messageService.AddAsync(userId, userName, newMessage.Text, newMessage.TopicId);
            }
            return RedirectToAction("DisplayTopic",new { id = newMessage.TopicId });
        }
        /// <summary>
        /// Delete message from topic. Only for admins
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteMessage(int messageId, int topicId)
        {
            await _messageService.DeleteByIdAsync(messageId);
            return RedirectToAction("DisplayTopic", new { id = topicId });
        }
        /// <summary>
        /// Delete topic. Only for admins.
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteTopic(int topicId)
        {
            await _topicService.DeleteByIdAsync(topicId);
            return RedirectToAction("AllTopics");
        }
        /// <summary>
        /// Redirects to Edit Message Form if current user is a message author
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult EditMessageForm(MessageDTO model)
        {
            if(User.Identity.GetUserId()!=model.UserForumId)
            {
                return new HttpStatusCodeResult(403);
            }
            return View(model);
        }
        /// <summary>
        /// Redirects to Edit Topic Form, if current user is a topic starter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult EditTopicForm(TopicDTO model)
        {
            if (User.Identity.GetUserId() != model.UserId)
            {
                return new HttpStatusCodeResult(403);
            }
            ModelState.Remove("Messages");
            return View(model);
        }
        /// <summary>
        /// Edits message
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> EditMessage(MessageDTO model)
        {
            var userName = User.Identity.Name;
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if(model.Text==null)
                {
                    ModelState.AddModelError("", "Message is empty!");
                }
                if(model.TopicId==0)
                {
                    ModelState.AddModelError("", "Empty Topic Id!");
                }
                if(model.UserForumId!=userId || String.IsNullOrEmpty(model.UserForumId))
                {
                    ModelState.AddModelError("", "Invalid\\Empty UserId");
                }
                if(model.UserName!=userName||String.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("", "Invalid\\Empty User Name");
                }
                if(model.Id==0)
                {
                    ModelState.AddModelError("", "Message Id is null");
                }
                if(String.IsNullOrEmpty(model.Date))
                {
                    ModelState.AddModelError("", "Date cant be empty!");
                }
                await _messageService.UpdateAsync(model);
            }
            return RedirectToAction("DisplayTopic", new { id = model.TopicId });
        }
        /// <summary>
        /// Edits topic
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> EditTopic(TopicDTO model)
        {
            var userName = User.Identity.Name;
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(model.Date))
                {
                    ModelState.AddModelError("", "Date cant be empty!");
                }
                if (model.UserId != userId || String.IsNullOrEmpty(model.UserId))
                {
                    ModelState.AddModelError("", "Invalid\\Empty UserId");
                }
                if (model.UserName != userName || String.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("", "Invalid\\Empty User Name");
                }
                if (model.Text == null)
                {
                    ModelState.AddModelError("", "Message should be filled");
                }
                if (model.Id == 0)
                {
                    ModelState.AddModelError("", "Empty Topic Id!");
                }
                await _topicService.UpdateAsync(model);
            }
            return RedirectToAction("DisplayTopic", new { id = model.Id });
        }
    }
}