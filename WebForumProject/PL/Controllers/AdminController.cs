﻿using BLL.Services.Interfaces;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    /// <summary>
    /// Controller for admin panel
    /// </summary>
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private IUserService _userService;
        private IAuthenticationManager _authenticationManager;
        private IUserRoleService _roleService;
        public AdminController(IUserService userService, IAuthenticationManager authenticationManager, IUserRoleService roleService)
        {
            _userService = userService;
            _authenticationManager = authenticationManager;
            _roleService = roleService;
        }
        /// <summary>
        /// Shows AdminPanel Page
        /// </summary>
        /// <returns> AdminPanel view with user model</returns>
        public ActionResult AdminPanel()
        {
            var model = _userService.GetAllUsers();
            return View(model);
        }
        /// <summary>
        /// Toggles admin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> AddRemoveAdmin(string id)
        {
            await _roleService.ToggleAdmin(id);
            return RedirectToAction("AdminPanel");
        }
        /// <summary>
        /// Delets User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteUser(string id)
        {
            await _userService.RemoveUser(id);
            return RedirectToAction("AdminPanel");
        }
    }
}