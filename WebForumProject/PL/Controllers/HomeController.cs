﻿using BLL.DTO;
using BLL.Services.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class HomeController : Controller
    {
        private ITopicService _topicService;
        private IMessageService _messageService;
        public HomeController(ITopicService topicService, IMessageService messageService)
        {
            _topicService = topicService;
            _messageService = messageService;
        }
        public ActionResult Index()
        {
            var allTopic = _topicService.GetAll().OrderByDescending(x=>x.Id).Take(3);
            var allMessages = _messageService.GetAll().OrderByDescending(x => x.Id).Take(3);
            var tuple = new Tuple<IEnumerable<TopicDTO>, IEnumerable<MessageDTO>>(allTopic, allMessages);
            return View(tuple);
        }

       
    }
}