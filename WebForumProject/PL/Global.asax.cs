﻿using BLL.DI;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using PL.DI;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
            //Registering DI
            NinjectModule bllModule = new BLLModule();
            NinjectModule servicesModule = new ServiceModule();
            var kernel = new StandardKernel(bllModule, servicesModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(resolver: new NinjectDependencyResolver(kernel));
           
            
        }
    }
   
}
