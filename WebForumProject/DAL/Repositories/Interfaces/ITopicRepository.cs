﻿using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface ITopicRepository : IRepository<Topic>
    {
        void DeleteAllUserTopics(string userId);
        IEnumerable<Topic> GetPagedTopics(int pageNumber, out int totalPages);
    }
}
