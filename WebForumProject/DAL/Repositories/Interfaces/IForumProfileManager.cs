﻿using DAL.Entities.UserEntities;
using System;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IForumProfileManager : IDisposable
    {
        void Create(ForumProfile item);
        Task Delete(ForumProfile item);
    }
}
