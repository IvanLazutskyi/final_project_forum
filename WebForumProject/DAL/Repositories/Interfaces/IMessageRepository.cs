﻿using DAL.Entities;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
        Task<int> AddAsyncMessageToTopic(Message entity, int id);
        void DeleteAllUserMessages(string userId);
    }
}
