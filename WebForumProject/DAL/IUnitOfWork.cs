﻿using DAL.Identity;
using DAL.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork : IDisposable
    {
        ForumUserManager UserManager { get; }
        IForumProfileManager ClientManager { get; }
        ForumRoleManager RoleManager { get; }
        IMessageRepository MessageRepository { get; }
        ITopicRepository TopicRepository { get; }
        Task SaveAsync();
    }
}
