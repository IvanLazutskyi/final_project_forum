﻿using DAL.EF;
using DAL.Entities.UserEntities;
using DAL.Identity;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Unit of work, wich is point of connection to DAL
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ForumContext _db;
        private readonly ForumUserManager _userManager;
        private readonly ForumRoleManager _roleManager;
        private readonly IForumProfileManager _forumProfileManager;
        private readonly IMessageRepository _messageRepository;
        private readonly ITopicRepository _topicRepository;
        public UnitOfWork()
        {
            _db = new ForumContext();
            
        }
        public ForumUserManager UserManager => _userManager ?? new ForumUserManager(new UserStore<ForumUser>(_db));
        public ForumRoleManager RoleManager => _roleManager ?? new ForumRoleManager(new RoleStore<ForumRole>(_db));

        public IForumProfileManager ClientManager => _forumProfileManager??new ForumProfileManager(_db);

        public IMessageRepository MessageRepository => _messageRepository?? new MessageRepository(_db);

        public ITopicRepository TopicRepository => _topicRepository??new TopicRepository(_db);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _userManager.Dispose();
                    _roleManager.Dispose();
                    _forumProfileManager.Dispose();
                    
                }
                this.disposed = true;
            }
        }
        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
            
        }
    }
}
