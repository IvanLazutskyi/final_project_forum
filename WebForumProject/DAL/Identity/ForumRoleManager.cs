﻿using DAL.Entities.UserEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Identity
{
    public class ForumRoleManager : RoleManager<ForumRole>
    {
        public ForumRoleManager(RoleStore<ForumRole> store)
                   : base(store)
        { }
    }
}
