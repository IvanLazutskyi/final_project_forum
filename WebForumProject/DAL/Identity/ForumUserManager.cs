﻿using DAL.Entities.UserEntities;
using Microsoft.AspNet.Identity;

namespace DAL.Identity
{
    public class ForumUserManager:UserManager<ForumUser>
    {
        public ForumUserManager(IUserStore<ForumUser> store)
                : base(store)
        {
        }
    }
}
