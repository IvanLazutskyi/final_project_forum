﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Entities.UserEntities
{
    public class ForumUser : IdentityUser
    {
        public virtual ForumProfile ForumProfile { get; set; }
    }
}
