﻿using DAL.Entities.UserEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// Topic Entity
    /// </summary>
    public class Topic : BaseEntity
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public string Text { get; set; }
        public virtual ForumUser ForumUser { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
