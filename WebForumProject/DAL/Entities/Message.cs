﻿using DAL.Entities.UserEntities;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// Message entity
    /// </summary>
    public class Message : BaseEntity
    {
        
        public string Date { get; set; }
        public string Text { get; set; }
        public virtual Topic Topic { get; set; }
        public virtual ForumUser ForumUser { get; set; }
    }
}
